//
//  CurrencyViewController.swift
//  CurrencyConverter
//
//  Created by Akash Bakshi on 2018-06-24.
//  Copyright © 2018 Akash Bakshi. All rights reserved.
//

import UIKit

class CurrencyViewController: UITableViewController {
    @IBOutlet var tbCurrency: UITableView!
    
    @IBOutlet weak var lbCurrencyTitle: UILabel!
    var currRates: Dictionary<String,AnyObject>!
    var currencies: CurrencyModel = CurrencyModel()
    
    var task: URLSessionDataTask!
    var tmpData: Data!
    var numRows: Int = 0
    
    
    func GetInfoFromRequest(){
        let tmpCurrency = CurrencyModel()
        
        let url = URL(string: "http://apilayer.net/api/live?access_key=ded8d056d930e7440dab015a7c74c2ec&currencies=EUR,GBP,CAD,AUD,%20JPY,CHF,INR&source=USD&format=1")
        
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            guard let dataResponse = data, error == nil else{return}
            
            do{
                let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse, options: [])
                
                if let json = jsonResponse as? Dictionary<String,AnyObject>{
                    self.currRates = json
                    //print(json)
                    if let base = json["source"] as AnyObject? as? String{
                        
                        if let rates = json["quotes"] as? Dictionary<String, AnyObject>? {
                            
                            for _ in rates!{
                                self.numRows += 1
                                tmpCurrency.SetAcronym(acr: "USD")
                                tmpCurrency.SetRates(rate: rates!)
                            }
                            self.currencies = tmpCurrency
                            
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                                self.lbCurrencyTitle.text = "Currency Rates (\(base))"
                                
                            }
                            
                        }
                    }
                }
                
            }catch{
                print("Parsing error")
            }
            
            
        })
        task.resume()
        self.task = task
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbCurrency.delegate = self
        tbCurrency.dataSource = self
        self.tbCurrency.contentInset = UIEdgeInsetsMake(0, -5, 0, 0)
        self.currRates = Dictionary<String,AnyObject>()
        
        GetInfoFromRequest()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        
        return numRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CustomCurrencyCellTableViewCell = self.tbCurrency.dequeueReusableCell(withIdentifier: "cell") as! CustomCurrencyCellTableViewCell
        cell.lbCurrencyName.text = currencies.GetKey(index: indexPath.row)
        cell.lbCurrencyRate.text = "\(currencies.GetRates(index: indexPath.row))"
        
        return cell
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
