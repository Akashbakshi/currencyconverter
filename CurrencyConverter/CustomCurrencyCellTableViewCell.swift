//
//  CustomCurrencyCellTableViewCell.swift
//  CurrencyConverter
//
//  Created by Akash Bakshi on 2018-06-24.
//  Copyright © 2018 Akash Bakshi. All rights reserved.
//

import UIKit

class CustomCurrencyCellTableViewCell: UITableViewCell {
    @IBOutlet weak var lbCurrencyName: UILabel!
    @IBOutlet weak var lbCurrencyRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
