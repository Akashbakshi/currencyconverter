//
//  CurrencyModel.swift
//  CurrencyConverter
//
//  Created by Akash Bakshi on 2018-06-24.
//  Copyright © 2018 Akash Bakshi. All rights reserved.
//

import Foundation


class CurrencyModel {
     
    var exchangeName: String!
    var acronym: Any!
    var rates: Dictionary<String,AnyObject>!
    
    init(acr: Any?, rate:Dictionary<String,AnyObject>) {
        acronym = acr
        rates = rate
    }
    
    init(){
        
    }
    
    public func SetAcronym(acr: String){
        self.acronym = acr
    }

    public func SetRates(rate: Dictionary<String,AnyObject>){
        self.rates = rate
    }
    
    public func GetKey(index: Int)->String{
        var tmpKey:String = Array(self.rates)[index].key
        tmpKey = tmpKey.replacingOccurrences(of: "USD", with: "")
        return tmpKey
    }
    public func GetRates(index: Int)->String{
        let curr = Array(self.rates)[index].value as! NSNumber
       
        return String(format:"%.4f",curr.floatValue)
    }
}
